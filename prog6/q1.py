from heapq import *
import sys
import numpy as np



def djikstra(source, edgemap, edgelist):
	# print edges
	d={}
	elem=[0,source,"ok"]
	edgemap[source]=elem
	Q=[]
	# print "Q=",Q
	# print "edgelist=",len(edgelist)
	Q.append( elem )
	for node in nodes:
		if (node!=source):
			elem=[sys.maxint, node, "ok"]
			edgemap[node]=elem
			Q.append(elem)
	# print "len(Q)=",len(Q)
	heapify(Q)

	while (len(Q)>0):
		# print "size=",len(Q)
		# print edgemap
		dist_u,vertex_u,status=heappop(Q)
		# print dist_u," ", vertex_u, " ", status
		# print "vertex=",vertex_u
		if (status=="removed"):
			continue
		else:
			for edge_u_v in edgelist[vertex_u]:

				vertex_v=edge_u_v[0]

				length_u_v=edge_u_v[1]

				alt=dist_u+length_u_v

				dist_v=edgemap[vertex_v][0]

				if (alt<dist_v):
					newelem=[alt,vertex_v,"ok"]
					oldelem=edgemap.pop(vertex_v)
					oldelem[2]="removed"
					edgemap[vertex_v]=newelem
					heappush(Q,newelem)
		
	return edgemap

def bellkey(i,v):
	return "("+i,","+v+")"

def bellman_ford(source, nodes, edges):
	print "total nodes=",len(nodes)
	for node in nodes:
		if (node!=source):
			bell_solutions[0][node-1]=sys.maxint

	# print edges

	# print bell_solutions
	# print "init"
	for i in range(1,len(nodes)+1):
		print "Iteration ",i
		for node in nodes:
			# print "\n node=",node
			same_sub=bell_solutions[i-1][node-1]
			related_min_sub=sys.maxint
			if node in edges:
				for edge in edges[node]:
					w=edge[0]
					# print "edge=",edge," i-1=",i-1," w-1=",w-1
					cur_min=bell_solutions[i-1][w-1]
					if (cur_min!=sys.maxint):
						cur_min=cur_min+edge[1]
					if (cur_min<related_min_sub):
						related_min_sub=cur_min
			
			bell_solutions[i][node-1]=min(same_sub, related_min_sub)	
		cmp_vector=bell_solutions[i-1]==bell_solutions[i]
		if (cmp_vector.all()):
			return bell_solutions[i]	
		# print bell_solutions
	print bell_solutions[len(nodes)-1]
	print bell_solutions[len(nodes)]
	return bell_solutions[len(nodes)]

file=open("large.txt")
first_line=file.readline()
num_nodes=int(first_line.split(" ")[0])

edges={}
nodes=set([])
entrymap={}
bell_edges={}

for line in file:
	parts=line.split(" ")
	node1=int(parts[0])
	node2=int(parts[1])
	cost=int(parts[2])
	if (node1 not in edges):
		edges[node1]=[]

	edges[node1].append((node2,cost))

	if (node2 not in edges):
		edges[node2]=[]	
	edges[node2].append((node1,cost))	
	nodes.add(node1)
	nodes.add(node2)
	
	if (node2 not in bell_edges):
		bell_edges[node2]=[]
	bell_edges[node2].append((node1, cost))

for node in nodes:
	if (node not in bell_edges):
		bell_edges[node]=[]

	bell_edges[node].append((num_nodes+1, 0))

bell_nodes=nodes.copy()
bell_nodes.add(num_nodes+1)

# djikstra(1,entrymap)

bell_solutions=np.zeros( (len(bell_nodes)+1,len(bell_nodes)), dtype=np.int)

bell_output=bellman_ford(num_nodes+1,bell_nodes,bell_edges)

bell_nodes.clear()
edges.clear()
# bell_solutions.clear()

# print bell_output

# file.close()
file.seek(0,0)

print "nodes size=",len(nodes)

file.readline()
ssp_edges={}
for line in file:
	parts=line.split(" ")
	node1=int(parts[0])
	node2=int(parts[1])
	cost=int(parts[2])
	if (node1 not in ssp_edges):
		ssp_edges[node1]=[]

	newcost=cost + bell_output[node1-1] - bell_output[node2-1]
	if (newcost<0):
		print "error! cost is ", newcost," for edge ",line
		exit()
	ssp_edges[node1].append([node2,newcost])

	if (node2 not in ssp_edges):
		ssp_edges[node2]=[]	
	ssp_edges[node2].append([node1,newcost])

# print "ssp_edges=",ssp_edges

# print "bell_output=",bell_output


file.seek(0,0)
file.readline()

clean_edges={}
for line in file:
	parts=line.split(" ")
	node1=int(parts[0])
	node2=int(parts[1])
	cost=int(parts[2])
	if (node1 not in clean_edges):
		clean_edges[node1]=[]
	clean_edges[node1].append([node2,cost])


min_path=sys.maxint
for node in nodes:
	print "SSP for node ",node
	entrymap2={}
	output=djikstra(node,entrymap2,ssp_edges)

	edge_costs={}
	# print output
	for key,triple in output.iteritems():
		# if (key not in edge_costs):
		# 	edge_costs[key]={}
		edge_costs[key]=triple[0]
	# print "old costs=", edge_costs

	for key in edge_costs:
		newcost=edge_costs[key]-bell_output[node-1]+bell_output[key-1]
		if (newcost<min_path):
			min_path=newcost

	# print "new costs=",edge_costs
	

# print clean_edges
print "min_path=",min_path
