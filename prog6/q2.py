from heapq import *
import sys
import numpy as np


def init1(matrix, nodes, edges):
	for node1 in nodes:
		matrix[node1][node1][0]=0

	for key in edges:
		for edge in edges[key]:
			matrix[key][edge[0]][0]=edge[1]


def init2(matrix, nodes, edges):
	for key in edges:
		for edge in edges[key]:
			matrix[key][edge[0]][0]=1	

def function1(matrix, i, j, k):
	rightvalue=0
	if (matrix[i][k][k-1]==sys.maxint):
		rightvalue=sys.maxint
	elif (matrix[k][j][k-1]==sys.maxint):
		rightvalue=sys.maxint
	else:
		rightvalue=matrix[i][k][k-1]+matrix[k][j][k-1]

	leftvalue=matrix[i][j][k-1]

	# print "rightvalue=",rightvalue
	# print "same solution=",matrix[i,j,k-1]
	# if (i==1 and j==1):
	# 	print "matrix[i][j][k-1]=",matrix[i][j][k-1]
	# 	print "matrix[i][k][k-1]=",matrix[i][k][k-1]
	# 	print "matrix[k][j][k-1]=",matrix[k][j][k-1]
	if (rightvalue>leftvalue):
		matrix[i][j][k]=leftvalue
	else:
		matrix[i][j][k]=rightvalue

def function2(matrix, i, j, k):
	rightvalue=0
	if (matrix[i][k][k-1]==sys.maxint):
		rightvalue=sys.maxint
	elif (matrix[k][j][k-1]==sys.maxint):
		rightvalue=sys.maxint
	else:
		rightvalue=matrix[i][k][k-1]*matrix[k][j][k-1]

	leftvalue=matrix[i][j][k-1]	

	if (leftvalue==sys.maxint or rightvalue==sys.maxint):
		matrix[i][j][k]=sys.maxint
	else:
		matrix[i][j][k]=leftvalue+rightvalue



file=open("g6.txt")
first_line=file.readline()
num_nodes=int(first_line.split(" ")[0])

edges={}
nodes=set([])
entrymap={}
# bell_edges={}

for line in file:
	parts=line.split(" ")
	node1=int(parts[0])
	node2=int(parts[1])
	cost=int(parts[2])
	if (node1 not in edges):
		edges[node1]=[]

	edges[node1].append((node2,cost))

	# if (node2 not in edges):
	# 	edges[node2]=[]	
	# edges[node2].append((node1,cost))	
	nodes.add(node1)
	nodes.add(node2)
	
	# if (node2 not in bell_edges):
	# 	bell_edges[node2]=[]
	# bell_edges[node2].append((node1, cost))

matrix=np.zeros( (len(nodes)+1,len(nodes)+1,len(nodes)+1), dtype=int)
# matrix.fill(sys.maxint)

init1(matrix, nodes, edges)
		

np.set_printoptions(linewidth=150)

print "\nIteration 0"
print "-------------"
print matrix[1:,1:,0]

for k in range(1,len(nodes)+1):

	for i in range(1,len(nodes)+1):
		for j in range(1,len(nodes)+1):

			function1(matrix, i, j , k)
			# rightvalue=0
			# if (matrix[i][k][k-1]==sys.maxint):
			# 	rightvalue=sys.maxint
			# elif (matrix[k][j][k-1]==sys.maxint):
			# 	rightvalue=sys.maxint
			# else:
			# 	rightvalue=matrix[i][k][k-1]+matrix[k][j][k-1]

			# leftvalue=matrix[i][j][k-1]

			# if (rightvalue>leftvalue):
			# 	matrix[i][j][k]=leftvalue
			# else:
			# 	matrix[i][j][k]=rightvalue
			

	print "\nIteration ",k
	print "-------------"
	print matrix[1:,1:,k]
	# if k==1:
	# 	sys.exit()

# print matrix