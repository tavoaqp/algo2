import numpy as np
import sys


file=open("knapsack_big.txt")
first_line=file.readline()
knapsack_size=int(first_line.split(" ")[0])
num_items=int(first_line.split(" ")[1])
sys.setrecursionlimit(knapsack_size)

weights=[]
values=[]
for line in file:
	tmp=line
	if "\n" in tmp:
		tmp=tmp[0:len(tmp)-1]
	weights.append(int(tmp.split(" ")[1]))
	values.append(int(tmp.split(" ")[0]))


solutions={}
def recursive(i,x):
	# print "i=",i," x=",x
	if (i==0):
		return 0
	else:
		mykey=str(i)+","+str(x)
		if (mykey not in solutions):
			# key1=str(i-1)+","+str(x)
			# key2=str(i-1)+","+str(x-weights[i-1])
			maxsol=0
			# print mykey, " calling solution ",str(i-1),",",str(x)
			key=str(i-1)+","+str(x)
			if (key not in solutions):
				maxsol=recursive(i-1,x)
			else:
				maxsol=solutions[key]
			if (weights[i-1]<=x):
				key=str(i-1)+","+str(x-weights[i-1])
				if (key not in solutions):
					maxsol=max(maxsol,recursive(i-1,x-weights[i-1])+values[i-1])
				else:
					maxsol=max(maxsol,solutions[key]+values[i-1])
				# print mykey, " calling solution ",str(i-1),",",str(x-weights[i-1])
				
			
			
			solutions[mykey]=maxsol
			# print "calculated solution ",mykey, "=",solutions[mykey]
		# else:
			# print "stored solution ",mykey,"=",solutions[mykey]
		return solutions[mykey]


recursive(num_items,knapsack_size)
# print "solutions=",solutions
finalkey=str(num_items)+","+str(knapsack_size)

print solutions[finalkey]