file=open("knapsack1.txt")
first_line=file.readline()
knapsack_size=int(first_line.split(" ")[0])
num_items=int(first_line.split(" ")[1])

weights=[]
values=[]
for line in file:
	tmp=line
	if "\n" in tmp:
		tmp=tmp[0:len(tmp)-1]
	weights.append(int(tmp.split(" ")[1]))
	values.append(int(tmp.split(" ")[0]))

A = [[0 for x in xrange(knapsack_size+1)] for x in xrange(num_items+1)]
# print values
# print weights
for i in range(1,num_items+1):
	for x in range(0,knapsack_size+1):
		

		if (weights[i-1]>x):
			A[i][x]=A[i-1][x]
		else:
			A[i][x]=max(A[i-1][x],values[i-1]+A[i-1][x-weights[i-1]])

		# if (i==1 and x==3):
		# 	print values[i-1]
		# 	print weights[i-1]
		# 	print "i=",i," -> ",A[i][:]

# for i in xrange(num_items+1):
# 	print i,"=>",A[i]
print A[num_items][knapsack_size]