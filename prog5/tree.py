import numpy as np
import sys

n=7
p=[0,0.05,0.4,0.08,0.04,0.1,0.1,0.23]
A=np.zeros((n+1,n+1))

def optimal(i,j):
	if (i>j):
		return 0
	if (i==j):
		return p[i]

	sum=0
	for k in range(i,j+1):
		sum=sum+p[k]

	min=sys.maxint
	for r in range(i,j+1):
		tree1=optimal(i,r-1)
		tree2=optimal(r+1,j)
		if (tree1+tree2+sum<min):
			min=tree1+tree2+sum

	return min


print optimal(1,n)

# answer 1: If a vertex is excluded from the optimal solution of a subproblem, then it is excluded from the optimal solutions of all bigger subproblems.
# answer 5: 2.18