from heapq import *
from multimap import *

def convert(bitstring):
	power=0
	number=0
	for bit in bitstring:
		number=number+int(bit)*pow(2,power)
		power=power+1
	return number

def turnbit(bit):
	if (int(bit)):
		return "0"
	else:
		return "1"

def combination1(bitstring):
	combinations=[]
	origcopy=bitstring.split(" ")
	for idx in range(0,len(origcopy)):
		bitcopy=bitstring.split(" ")
		bitcopy[idx]=turnbit(origcopy[idx])
		combinations.append(" ".join(bitcopy))
	return combinations

def combination2(bitstring):
	combinations=[]
	origcopy=bitstring.split(" ")
	for idx1 in range(0,len(origcopy)):
		combination1=bitstring.split(" ")
		combination1[idx1]=turnbit(origcopy[idx1])
		tmpcomb=" ".join(combination1)
		for idx2 in range(idx1+1,len(origcopy)):
			combination2=tmpcomb.split(" ")
			combination2[idx2]=turnbit(combination2[idx2])
			combinations.append(" ".join(combination2))
	return combinations


file=open("clustering_big.txt")
num_nodes=int(file.readline().split(" ")[0])

nodes=set([])
for line in file:
	# print line
	bitstring=line
	if ('\n' in bitstring):
		bitstring=bitstring[0:len(bitstring)-1]

	bitstring=bitstring.strip()
	bitstring=bitstring.split(" ")
	nodes.add(convert(bitstring))

file.close()

file=open("clustering_big.txt")
file.readline()
edges1=MutableMultiMap([])
print "finding edges1"
nodecount=1
for line in file:
	bitstring=line
	if ("\n" in bitstring):
		bitstring=line[0:len(line)-1]
	bitstring=bitstring.strip()
	# bitstring=" ".join(bitstring[0:len(bitstring)])
	# print bitstring
	# print "ORIG=",bitstring
	bitnumber=convert(bitstring.split(" "))
	# print "bitnumber=",bitnumber
	combinations1=combination1(bitstring)
	# print combinations1
	for comb1 in combinations1:
		int1=convert(comb1.split(" "))
		if (int1 in nodes):			
			edges1.append((bitnumber,int1))
			edges1.append((int1,bitnumber))
	
	combinations2=combination2(bitstring)
	# print combinations2
	for comb2 in combinations2:
		int2=convert(comb2.split(" "))
		if (int2 in nodes):
			edges1.append((bitnumber,int2))
			edges1.append((int2,bitnumber))

	print "nodecount=",nodecount
	nodecount=nodecount+1
	# print nodes
	# print edges1

print "\nfinding components\n"
k=0
while len(nodes)>0:
	cur_root=nodes.pop()
	# print "cur_root=",cur_root
	stack=[cur_root]
	discovered=set([])
	while len(stack)>0:
		cur_node=stack.pop()
		# print "cur_node=",cur_node
		if (cur_node not in discovered):
			discovered.add(cur_node)
			for next_node in edges1.getall(cur_node):
				stack.append(next_node)
		# print "discovered=",discovered
		# print "stack=",stack,"\n"
	k=k+1
	nodes=nodes-discovered

print "k=",k


