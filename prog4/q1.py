from heapq import *

file=open("clustering1.txt")
num_nodes=int(file.readline())
leaders={}
clusters={}
for idx in range(1,num_nodes+2):
	leaders[idx]=idx
	clusters[idx]=list([idx])

edges=[]
for line in file:
	parts=line.split(" ")
	edges.append( (int(parts[2]), int(parts[0]), int(parts[1])) ) 

heapify(edges)

k=num_nodes
target_k=4
print leaders
while k>target_k:
	min_edge=heappop(edges)
	node1=min_edge[1]
	node2=min_edge[2]
	print "\nmin_edge",min_edge
	if (leaders[node1]!=leaders[node2]):
		if (len(clusters[leaders[node1]])>len(clusters[leaders[node2]])):
			print "Joining ",clusters[leaders[node2]]," with ",clusters[leaders[node1]]
			for idx in clusters[leaders[node2]]:
				leaders[idx]=leaders[node1]
				clusters[leaders[node1]].append(idx)	
			clusters[leaders[node1]]=list(set(clusters[leaders[node1]]))
			
		else:
			print "Joining ",clusters[leaders[node1]]," with ",clusters[leaders[node2]]
			for idx in clusters[leaders[node1]]:
				leaders[idx]=leaders[node2]
				clusters[leaders[node2]].append(idx)
			clusters[leaders[node2]]=list(set(clusters[leaders[node2]]))
			
		k=k-1


clusters_final={}
for node in range(1,num_nodes+1):
	if leaders[node] not in clusters_final:
		clusters_final[leaders[node]]=list([])
	clusters_final[leaders[node]].append(node)


# print "FINAL"
# print clusters_final
# print "\nLEADERS"
# print leaders
print "min distance"
print heappop(edges)[0]
	
