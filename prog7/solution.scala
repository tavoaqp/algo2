// import scala.collection.mutable._
import scala.collection.mutable._

object Solution {

  def main(arg: Array[String]) {
    leftside()
    rightside()

  }

  def rightside():Double={
  	var lines = io.Source.fromFile("tsp_right.txt").getLines()
    var numNodes = lines.next().toInt
    var points = ArrayBuffer[Tuple2[Float, Float]]()

    while (lines.hasNext) {
      var line = lines.next
      var val1 = line.split(" ")(0).toFloat
      var val2 = line.split(" ")(1).toFloat
      points += Tuple2(val1, val2)
    }

    // System.out.println(points)

    var cost = tsp(13, points)

    System.out.println(cost)

    cost
  }

  def leftside():Double={
  	var lines = io.Source.fromFile("tsp_left.txt").getLines()
    var numNodes = lines.next().toInt
    var points = ArrayBuffer[Tuple2[Float, Float]]()

    while (lines.hasNext) {
      var line = lines.next
      var val1 = line.split(" ")(0).toFloat
      var val2 = line.split(" ")(1).toFloat
      points += Tuple2(val1, val2)
    }

    // System.out.println(points)

    var cost = tsp(11, points)

    System.out.println(cost)

    cost
  }

  def printDist(distances: Array[Array[Float]]) {
    for (row <- distances.zipWithIndex) {
      System.out.print("(i=" + row._2 + ") -> ")
      for (elem <- row._1)
        System.out.print("%1.1f".format(elem) + " ")
      System.out.print("\n")
    }
  }

  def tsp(endNode: Int, points: ArrayBuffer[Tuple2[Float, Float]]): Float = {

    var numNodes = points.size
    // System.out.println("numNodes=" + numNodes)
    var startNode = 0

    var distances = Array.ofDim[Float](numNodes, numNodes)

    for (x1 <- 0 to points.size - 1) {
      for (x2 <- 0 to points.size - 1) {
        var distance = math.sqrt(math.pow(points(x1)._1 - points(x2)._1, 2) + math.pow(points(x1)._2 - points(x2)._2, 2))
        distances(x1)(x2) = distance.toFloat
      }
    }

    // printDist(distances)

    var nodes = new BitSet()
    nodes ++= Range(0, numNodes.toInt).toList
    // System.out.println("nodes=" + nodes)
    var setIndices = new HashMap[BitSet, Int]()

    var setCount = 0
    for (m <- 1 to numNodes) {
      // System.out.println("generating sets of size " + m)
      var it = nodes.subsets(m)
      while (it.hasNext) {
        var s = it.next()
        if (s.contains(startNode)) {
          setIndices += Tuple2(s, setCount)
          setCount += 1
        }
      }
    }

    // System.out.println(setCount)
    // System.out.println(setIndices)

    var matrix = Array.ofDim[Float](setCount, numNodes)

    for (pair <- setIndices) {
      var set = pair._1
      var index = pair._2
      if (set.contains(startNode) && set.size == 1) {
        matrix(index)(0) = 0
      } else {
        matrix(index)(0) = Float.MaxValue
      }
    }
    // System.out.println("starting")
    // printDist(matrix)

    for (m <- 2 to numNodes) {
      var it = nodes.subsets(m)
      // System.out.println("\nm=" + m)
      while (it.hasNext) {
        var S = it.next()
        if (S.contains(startNode)) {

          var sIndex = setIndices(S)
          // System.out.println("S=" + S + " --> " + sIndex)
          for (j <- S.filter(_ != startNode)) {
            var minCost = Float.MaxValue
            for (k <- S.filter(_ != j)) {
              var tmpSet = S - j
              var tmpIndex = setIndices(tmpSet)
              // System.out.println("(j="+j+") ##### > Calling set: " + tmpSet + " --> " + tmpIndex+" k="+k+" matrix("+tmpIndex+")("+k+")="+"%.1f".format(matrix(tmpIndex)(j))+" distances("+k+")("+j+")="+"%.1f".format(distances(k)(j)))
              var curCost = matrix(tmpIndex)(k) + distances(k)(j)
              if (curCost < minCost) {
                minCost = curCost
              }
            }

            matrix(sIndex)(j) = minCost
            // System.out.println("##### > min=" + minCost)
          }
        }
      }
      // printDist(matrix)
    }
    // System.out.println("\n")

    // printDist(matrix)

    // System.out.println("\n")

    var globalMin = Float.MaxValue
    for (j <- 2 to numNodes) {
      var setIt = nodes.subsets(numNodes)
      while (setIt.hasNext) {
        var nextSet = setIt.next()
        if (nextSet.contains(startNode)) {
          var tmpIndex = setIndices(nextSet)
          var curMin = matrix(tmpIndex)(j - 1) + distances(j - 1)(endNode)
          if (curMin < globalMin) {
            globalMin = curMin
          }
        }
      }

    }

    globalMin

  }

}