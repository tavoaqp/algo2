import scala.collection.mutable._

object Solution {

  var props = HashSet[HashMap[Int, Int]]()
  var numVars = 0
  var vars = ArrayBuffer[Int]()

  def main(arr: Array[String]) {
    var lines = scala.io.Source.fromFile(arr(0)).getLines()
    numVars = lines.next.toInt
    // //System.out.println("numVars=" + numVars)
    for (idx <- 1 to numVars + 1)
      vars += (-1)

    for (line <- lines) {

      var values = line.split(" ")
      var bitset = HashMap[Int, Int]()
      for (value <- values) {
        var tmpVal = 0
        if (value.toInt > 0)
          tmpVal = 1

        bitset += Pair((math.abs(value.toInt)), tmpVal)
        // if (values.size>1)
        // 	bitset+=Pair((math.abs(value.toInt)),(value.toInt>0))

      }
      props += bitset

    }

    // //System.out.println("Start clauses=" + props)

    // //System.out.println("Start Vars=" + vars)

    var stack = new Stack[Pair[Int, Int]]()
    stack.push(Pair(0, 0))
    while (!stack.isEmpty) {
      var top = stack.pop()


      // //System.out.println("\n############# Current problem=" + top.toString)
      vars(top._1) = top._2

      if (top._2 >= 0) {
      	var problems = genProblem(top)
      	// //System.out.println("Generated subproblems=" + problems.toString)
      	// //System.out.println("Before Vars="+vars)
      	// var allProblemsOkCount=problems.size
      
        for (problem <- problems) {
          if (problem._2 < 0) {
            stack.push(problem)
          } else {
            // if (problem._2==1)
            //System.out.println("\n+++++++++++?? Testing subproblem=" + problem.toString)
            vars(problem._1) = problem._2
            // //System.out.println("After Vars="+vars)
            // else
            // 	vars(problem._1)=0
            var evalValue = evalClauses()
            //System.out.println("Result from subproblem=" + evalValue)
            if (evalValue == 1) {
              System.out.println("SAT: Variables " + vars.slice(1, vars.size))
              vars.clear()
              props.clear()
              return
            } else if (evalValue == 0) {
              vars(problem._1) = (-1)
              // allProblemsOkCount -= 1
            } else {
              stack.push(problem)
            }
          }
        }
      } else {
        vars(top._1) = (-1)
      }
      // if (allProblemsOkCount==0)
      // 	vars(top._1)=(-1)
      // //System.out.println("allProblemsOkCount=" + allProblemsOkCount)

    }
    vars.clear()
    props.clear()
    System.out.println("NOT SAT")
  }

  def genProblem(assign: Pair[Int, Int]): ArrayBuffer[Pair[Int, Int]] = {
    //System.out.println("\n1. Generating problems...")
    //System.out.println("1. vars=" + vars)
    var varId = assign._1
    var varValue = assign._2
    if (varId == 0) {
      // //System.out.println("numVars="+numVars)
      varId = scala.util.Random.nextInt(numVars - 1)
      varValue = vars(varId)
    }
    var minCount = Int.MaxValue
    var minClause: Option[HashMap[Int, Int]] = None

    for (clause <- props) {
      var curCount = 0
      var allTrue = false
      for (pair <- clause) {

        var varId = pair._1
        var varOp = pair._2

        if (vars(varId) == (-1)) {
          curCount += 1
          // //System.out.println("A. curCount="+curCount)
        } else {
          var finalValue = vars(varId)
          if (varOp < 1)
            finalValue = math.abs(1 - finalValue)
          if (finalValue > 0) {
            allTrue = true
            // //System.out.println("B. curCount="+curCount)
          }
        }

      }
      if (allTrue && curCount == 0)
        curCount = Int.MaxValue
      //System.out.println("Free vars=" + curCount + " for clause=" + clause)
      if (curCount > 0 && curCount < minCount) {

        minCount = curCount
        minClause = Some(clause)
        //System.out.println("Minimum clause=" + minClause)
      }
    }

    var output = ArrayBuffer[Pair[Int, Int]]()
    if (minCount > 0 && !minClause.isEmpty) {

      for (pair <- minClause.get) {
        var varId = pair._1
        if (vars(varId) == (-1)) {          
          output += Pair(varId, -1)
          output += Pair(varId, 1)
          output += Pair(varId, 0)                    
          return output
        }
      }
    }

    output
  }

  def evalClauses(): Int = {
    //System.out.println("\n2. Evaluating.....")
    //System.out.println("2. vars=" + vars)
    var finalValue: Option[Int] = None

    for (clause <- props) {

      var curClauseValue: Option[Int] = None
      for (pair <- clause) {
        var varId = pair._1
        var varOp = pair._2
        if (vars(varId) == (-1)) {
          return -1
        } else {

          var finalValue = vars(varId)
          if (varOp < 1) {
            finalValue = math.abs(1 - finalValue)
          }
          if (curClauseValue.isEmpty) {
            curClauseValue = Some(finalValue)
          } else {
            curClauseValue = Some(curClauseValue.get | finalValue)
          }

        }
      }

      if (finalValue.isEmpty) {
        finalValue = curClauseValue
      } else {
        finalValue = Some(finalValue.get & curClauseValue.get)
      }
      //System.out.println("Clause=" + clause + " has value " + finalValue)
    }
    return finalValue.get
  }

}
