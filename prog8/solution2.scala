import scala.collection.mutable._
import scala.collection.JavaConversions._

object Solution2 {

  var props = ArrayBuffer[HashMap[Int, Int]]()
  var numVars = 0
  var vars = ArrayBuffer[Int]()

  def main(arr: Array[String]) {
    var lines = scala.io.Source.fromFile(arr(0)).getLines()
    numVars = lines.next.toInt
    System.out.println("numVars=" + numVars)
    for (idx <- 1 to numVars + 1)
      vars += (-1)

    for (line <- lines) {

      var values = line.split(" ")
      var bitset = HashMap[Int, Int]()
      for (value <- values) {
        var tmpVal = 0
        if (value.toInt > 0)
          tmpVal = 1

        bitset += Pair((math.abs(value.toInt)), tmpVal)
        // if (values.size>1)
        // 	bitset+=Pair((math.abs(value.toInt)),(value.toInt>0))

      }
      props += bitset

    }

    var logN=(math.log10(numVars)/math.log10(2)).toInt
    System.out.println("logN="+logN)
    var squareN=2*math.pow(numVars,2).toLong
    System.out.println("squareN="+squareN)
    for (x <- 1 to logN){
      chooseRandomAssign()
      
      var idx=1L
      while ( idx < squareN){
        System.out.println("x="+x+" idx="+idx)
        var satisfied=evalClauses()
        if (satisfied==1)
        {
          System.out.println("SAT Vars:"+vars)
          vars.clear()
          props.clear()
          return
        }
        else{
          flipUnsatisfiedClause()
        }
        idx+=1L
      }
      
    }
    vars.clear()
          props.clear()
    System.out.println("UNSAT")

  }


  def flipUnsatisfiedClause()  {

    
    // System.out.println("selectedClauses="+selectedClauses)
    val unsatisfiedFunc:PartialFunction[HashMap[Int,Int],HashMap[Int,Int]]={
      case t if t.map( t => if (t._2>0) vars(t._1) 
                            else math.abs(1 - vars(t._1))
                      ).reduce(_ | _) < 1 => t      
    }
    var firstUnsatisfied=props.view.collectFirst( unsatisfiedFunc )
    System.out.println("firstUnsatisfied="+firstUnsatisfied)
    var selectProb=scala.util.Random.nextFloat
    if (!firstUnsatisfied.isEmpty){
      var keySet=firstUnsatisfied.get.keys

      if (selectProb>0.5f)
      {
        vars(keySet.head)=math.abs(1-vars(keySet.head))
      }
      else{
        if (keySet.size>1)
          vars(keySet.tail.head)=math.abs(1-vars(keySet.tail.head))
        else
          vars(keySet.head)=math.abs(1-vars(keySet.head))
      }
    }
  }

  def chooseRandomAssign(){
    // vars.foreach ( t => scala.util.Random.nextInt(2))
    for (idx <- 1 to numVars)
    {
      vars(idx)=scala.util.Random.nextInt(2)
    }
  }
  

  def evalClauses(): Int = {
    //System.out.println("\n2. Evaluating.....")
    //System.out.println("2. vars=" + vars)
    
    var optValue=props.view.map( clause => clause.map{ varPair => if (varPair._2>0) vars(varPair._1)
                                                else math.abs(1 - vars(varPair._1))
                                    }.reduce(_ | _)).reduce(_ & _)
    return optValue
    // System.out.println("optValue="+optValue)

    // var finalValue: Option[Int] = None

    // for (clause <- props) 
    // {

    //   var curClauseValue: Option[Int] = None
    //   for (pair <- clause) {
    //     var varId = pair._1
    //     var varOp = pair._2
    //     if (vars(varId) == (-1)) {
    //       return -1
    //     } else {

    //       var finalValue = vars(varId)
    //       if (varOp < 1) {
    //         finalValue = math.abs(1 - finalValue)
    //       }
    //       if (curClauseValue.isEmpty) {
    //         curClauseValue = Some(finalValue)
    //       } else {
    //         curClauseValue = Some(curClauseValue.get | finalValue)
    //       }

    //     }
    //   }

    //   if (finalValue.isEmpty) {
    //     finalValue = curClauseValue
    //   } else {
    //     finalValue = Some(finalValue.get & curClauseValue.get)
    //   }
    //   //System.out.println("Clause=" + clause + " has value " + finalValue)
    // }
    // return finalValue.get
  }

}
