import scala.collection.mutable._
import scala.collection.JavaConversions._

object Solution3 {

  def main(arr:Array[String]){
    var sol=new SCC()
    sol.solve(arr)
  }


  class SCC{

  var props = ArrayBuffer[HashMap[Int, Int]]()
  var numVars = 0
  var vars = ArrayBuffer[Int]()

  var graph = HashMap[Int,ArrayBuffer[Int]]()
  var inverseGraph = HashMap[Int,ArrayBuffer[Int]]()
  // var oGraph = HashMap[Int,ArrayBuffer[Int]]()
  var vertices=HashSet[Int]()
  var stack=new ArrayStack[Int]()
  var extendedVertices=HashSet[Int]()

  def dfs(u:Int, used:HashSet[Int], order: ArrayBuffer[Int]){
    used+=u
    for ( vertex <- graph(u)){
      if (!used.contains(vertex))
      {
        dfs(vertex, used, order)
      }
    }
    order+=u
  }

  def dfs2(u:Int, used:HashSet[Int], order: ArrayBuffer[Int]){
    used+=u
    for ( vertex <- inverseGraph(u)){
      if (!used.contains(vertex))
      {
        dfs2(vertex, used, order)
      }
    }
    order+=u
  }

  def scc(){
    
    System.out.println("Creating inverseGraph")
    for (key <- graph.keySet)
      inverseGraph+=Pair(key,ArrayBuffer[Int]())

    for (key <- graph.keySet){
      for ( node <- graph(key)){
        inverseGraph(node)+=key
      }
    }
    System.out.println("inverseGraph created")
    // System.out.println("theGraph="+theGraph)
    // System.out.println("\ninverseGraph="+inverseGraph)
    var count=0
    var used=HashSet[Int]()
    var order=ArrayBuffer[Int]()
    for ( vertex <- graph.keySet){
      count+=1
      // System.out.println("Calculating Depth search for vertex "+vertex)
      if (!used.contains(vertex)){
        dfs(vertex, used, order)
      }
      // System.out.println("Depth search calculated for vertex "+vertex)
      if (count%100==0)
      System.out.println("Count="+count)

    }

    used=HashSet[Int]()
    count=0
    for ( vertex <- order.reverse ){
      count+=1
      if (!used.contains(vertex)){
        var components=ArrayBuffer[Int]()
        // System.out.println("Calculating Components for vertex "+vertex)
        dfs2(vertex, used, components)
        // System.out.println("Vertex "+vertex+" has components "+components)
        // System.out.println("Components calculated for vertex "+vertex)
        
        for (comVertex <- components)
        {
          if (components.contains(-1*comVertex)){
            System.out.println("UNSAT")
            return
          }
        }
      }
      if (count%100==0)
      System.out.println("Com Count="+count)
    }
    System.out.println("SAT")



  }

  def solve(arr: Array[String]) {
    var lines = scala.io.Source.fromFile(arr(0)).getLines()
    numVars = lines.next.toInt

    System.out.println("numVars=" + numVars)
    for (idx <- 1 to numVars){
      vertices+=idx
      extendedVertices+=idx
      extendedVertices+=(-1)*idx
      if (!graph.contains(idx))
        graph+=Pair(idx,ArrayBuffer[Int]())
      if (!graph.contains((-1)*idx))
        graph+=Pair((-1)*idx,ArrayBuffer[Int]())
      // if (!oGraph.contains(idx))
      //   oGraph+=Pair(idx,ArrayBuffer[Int]())
      // if (!oGraph.contains((-1)*idx))
      //   oGraph+=Pair((-1)*idx,ArrayBuffer[Int]())
    }

    for (line <- lines) {

      var values = line.split(" ")
      var left=values(0).toInt
      var right=values(1).toInt

      var hypLeft=(-1)*left
      var hypRight=(-1)*right
      graph(hypLeft)+=right
      graph(hypRight)+=left
      // oGraph(right)+=hypLeft
      // oGraph(left)+=hypRight
    }

    scc()
    // System.out.println("\noGraph="+oGraph)


    }


  }
 
  //2sat1 => SAT
  //2sat2 => UNSAT
  //2sat3 => SAT
  //2sat4 => SAT
  //2sat5 => UNSAT
  //2sat6 => 
}
