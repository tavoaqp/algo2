#!/usr/bin/python
import random

file=open("edges.txt")
meta=file.readline().split(" ")

G=[]
V=set([])
for line in file:
	split=line.split(" ")
	node1=int(split[0])
	node2=int(split[1])
	weight=int(split[2])
	V.add(node1)
	V.add(node2)
	G.append( ( node1 , node2, weight ) )

X=set([1])
T=set([])
print "G="+str(G)


print "V="+str(V)
while(X!=V):
	print "\n"
	print "T="+str(T)
	print "X="+str(X)
	print "V="+str(V)
	min=None
	for edge in G:
		print "edge="+str(edge)
		
		if (edge[0] in X and edge[1] not in X) or (edge[1] in X and edge[0] not in X):			
			if min==None:
				min=edge
				print "min (init)="+str(min)
			else:
				if (min[2]>edge[2]):					
					print "old min="+str(min)
					min=edge
					print "new min="+str(edge)
	print "current min="+str(min)
	# exit()
	if (min!=None):
		T.add(min)
		if (min[0] in X and min[1] not in X):
			X.add(min[1])
		elif (min[1] in X and min[0] not in X):
			X.add(min[0])
print "X="+str(X)
print "V="+str(V)

sum=0
for edge in sorted(T, key=lambda obj: obj[0]):
	print "("+str(edge[0])+","+str(edge[1])+")"
	sum=sum+edge[2]

# print T
print "sum="+str(sum)


